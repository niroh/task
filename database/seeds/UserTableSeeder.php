<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'Admin',
                        'email' => 'a@a.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'admin',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
               
            
                    ]);
    }

}
