<h1>Create New Task</h1>
<form method = 'post' action="{{action('TaskController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">What task would you like to add?</label>
    <input type= "text" class = "form-control" name= "title">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Add">
</div>

</form>
