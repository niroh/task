
<a href = "{{route('mytasks', $id)}}"> My Tasks </a>
<h1>This is your Task list</h1>
    <ul>
        @foreach($tasks as $task)
        <li>
            id: {{$task->id}} title:{{$task->title}} 
            <a href= "{{route('tasks.edit', $task->id )}}"> Edit </a>
            @cannot('user') <a href= "{{route('delete', $task->id)}}"> Delete </a> @endcannot

            @if($task->status == 1)
            <a>Task is DONE!</a>
            @else
            @cannot('user') <a input type ='url' href= "{{route('update', $task->id)}}"> Mark as done </a>  @endcannot
            @endif

             
        @endforeach
        </li>        
    </ul>

    <a href="{{route('tasks.create')}}">Create New Task </a>

 